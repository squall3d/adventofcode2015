<?php
define('LB', "\n");

$containers = file('aoc.17.txt', FILE_IGNORE_NEW_LINES);
$container_count = count($containers);
echo 'Input contains ' . count($containers) . 'lines' . LB;
arsort($containers);
echo 'Start: ' . print_r($containers, TRUE) . LB;

//$combination = array_fill(0, count($containers), 0);
$combination = array_keys($containers);
var_dump($combination);
//pc_permute($combination);

$combinations_hash = [];
$combinations = [];
$capacity = 150;

$containers_required_min = 1000000000;
$containers_required_max = 4; // Got this from previous run.
$ways_that_works = 0;
$max = pow(2,$container_count); echo '$max:'.$max.LB;

for($i=0; $i<$max; $i++) {

  $c = strrev(str_pad(decbin($i),$container_count,'0',STR_PAD_LEFT));
  $c_arr = str_split($c);
  //arsort($c_arr);
  $total_capacity = 0;
  $containers_required = 0;

  for($j=0; $j<count($c_arr); $j++){
    //echo '$c_arr[$j]:'.$c_arr[$j].LB;
    if ($c_arr[$j] == '1') {
      $containers_required++;
      if ($containers_required > $containers_required_max) {
        $total_capacity = 0;
        break;
      }
      $total_capacity += $containers[$j]; //echo '$total_capacity:'.$total_capacity.LB;
      if ($total_capacity > $capacity) {
        $total_capacity = 0;
        break;
      }
    }
  }
  if ($total_capacity == $capacity/* && $containers_required == $containers_required_max*/) {
    echo $c.' works'.LB;
    if ($containers_required < $containers_required_min) {
      $containers_required_min = $containers_required;
    }
    $ways_that_works++;
  }else{
    //echo $c.''.LB;
  }
}

echo '$ways_that_works:'.$ways_that_works.LB;
echo '$containers_required_min:'.$containers_required_min.LB;

//recipe 4.26 of O'Reilly's "PHP Cookbook".
//function pc_permute($items, $perms = array( )) {
//  if (empty($items)) {
//    print join(' ', $perms) . "\n";
//  }  else {
//    for ($i = count($items) - 1; $i >= 0; --$i) {
//      $newitems = $items;
//      $newperms = $perms;
//      list($foo) = array_splice($newitems, $i, 1);
//      array_unshift($newperms, $foo);
//      pc_permute($newitems, $newperms);
//    }
//  }
//}