<?php
/**
 * Created by PhpStorm.
 * User: ryan
 * Date: 12/8/15
 * Time: 12:53 PM
 */

define('LB', "\n");

// Read as a string
//$raw = file_get_contents('aoc.8.txt');

// Or, read as array
$raw = file('aoc.8.ori.txt', FILE_IGNORE_NEW_LINES);
echo 'Starting with ' . count($raw) . LB;


//$hex_pattern = '/.*(\x[0-9a-f]{2}).*/';
//$hex_pattern = '/(HHHH[0-9a-f]{2})/';

$total = 0;

foreach($raw as $s) {
  $totalcode += strlen($s);
  echo "$s (" . strlen($s) . ")\n";

  //$s = substr($s, 1, strlen($s)-2);
  $s = str_replace('\\', '\\\\', $s);
  $s = str_replace('"', '\"', $s);


  //$s2 = str_replace('\x', 'HHHH', $s2);
//  $matches = [];
//  if (preg_match($hex_pattern, $s2, $matches)){
//    var_dump($matches);
//  }
//
//  $count = 0;
//  $s3 = preg_replace($hex_pattern, 'H', $s2, -1, $count); echo '$count:'.$count.LB;

  $s = '"' . $s . '"';

  $total += strlen($s);
  echo "$s (" . strlen($s) . ")\n";
}


echo "totalcode: $totalcode, total: $total, ans: " . ($totalcode-$total) . ".";