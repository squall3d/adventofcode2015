<?php
/**
 * Created by PhpStorm.
 * User: ryan
 * Date: 12/6/15
 * Time: 12:09 AM
 */

echo '<pre>';

$raw = file_get_contents('aoc.3.txt');
echo $raw;
$raw = str_split($raw);
//echo $raw;

$presents = [];

$x = 3000;
$y = 3000;
define('L', 6000);

function getCellID($_x, $_y) {
  return (max(0, $_y - 1) * L) + $_x - 1;
}

//echo getCellID($startX, $startY, $l);

$presents[getCellID($x, $y)]++;
$errors = 0;

foreach($raw as $dir) {
  if ($dir == '<') {
    $x--;
  }elseif ($dir == '^') {
    $y--;
  }elseif ($dir == '>') {
    $x++;
  }elseif ($dir == 'v') {
    $y++;
  }else{
    $errors++;
  }
  $cellId = getCellID($x, $y);
  echo '$cellId:' . $cellId . "\n";
  $presents[$cellId]++;
}

echo 'houses with at least 1 present: ' . countHousesAtLeastOnePresent($presents) . "\n";
echo 'Errors: ' . $errors . "\n";

function countHousesAtLeastOnePresent($houses) {
  $c = 0;
  $c2 = 0;
  foreach($houses as $key => $house) {
    if ($house > 0) {
      $c++;
      echo $key . "\n";
      if ($house > 1) {
        $c2++;
      }
    }
  }

  echo 'houses with at least 2 present: ' . $c2;
  return $c;
}


echo '</pre>';