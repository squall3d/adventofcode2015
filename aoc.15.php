<?php
define('LB', "\n");

$raw = file('aoc.15.txt', FILE_IGNORE_NEW_LINES);
echo 'Input contains ' . count($raw) . 'lines' . LB;

//Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
$pattern = '/([^ ]+): capacity ([-\d]+), durability ([-\d]+), flavor ([-\d]+), texture ([-\d]+), calories ([-\d]+)/';
$stats = ['capacity', 'durability', 'flavor', 'texture'];

$infos = [];

foreach ($raw as $line) {
  if (preg_match($pattern, $line, $matches)) {
    print_r($matches);
    //$infos[$matches[1]] = [
    $infos[] = [
      'name' => $matches[1],
      'capacity' => (int)$matches[2],
      'durability' => (int)$matches[3],
      'flavor' => (int)$matches[4],
      'texture' => (int)$matches[5],
      'calories' => (int)$matches[6],
    ];
  }
}

$first_ingredient = reset(array_keys($infos));

echo 'Start: ' . print_r($infos, TRUE) . LB;

//$combination = array_combine(array_keys($infos), array_fill(0, count($infos), 0));
$combination = array_fill(0, count($infos), 0); var_dump($combination);

$combinations_hash = [];
$combinations = [];
$max_score = 0;

//for($i=0; $i<100000000; $i++) {
//  $combination[3] = 0;
//
//  $combination[2]++;
//  if ($combination[2] > 100) {
//    $combination[2] = 0;
//    $combination[1]++;
//  }
//  if ($combination[1] > 100) {
//    $combination[1] = 0;
//    $combination[0]++;
//  }
//
//  $combination[3] = 100 - $combination[0] - $combination[1] - $combination[2];
//  if ($combination[3] < 0) {
//    continue;
//  }
//
//  $combination_string = $combination[0] .'|'. $combination[1] .'|'. $combination[2].'|'.$combination[3]; echo '$combination_string:'.$combination_string.LB;
//  $combinations_hash[$combination_string] = $combination;
//  $combinations[] = $combination;
//
//  $score = score($combination); echo '$score:'.$score.LB;
//  if ($score > $max_score){
//    $max_score = $score;
//  }
//
//  if ($combination[0] == 100) {
//    break;
//  }
//}

//var_dump($combinations_hash);

for($i1=0; $i1<100; $i1++) {
  for($i2=0; $i2<100; $i2++) {
    for($i3=0; $i3<100; $i3++) {

      $i4 = 100 - $i1 - $i2 - $i3;
      if ($i4 < 0) {
        continue;
      }

      $combination_string = $i1.'|'.$i2.'|'.$i3.'|'.$i4;
      echo $combination_string . LB;
      $combination = [$i1, $i2, $i3, $i4];
      list($score, $calories) = score($combination);
      echo '$score:'.$score.', $calories:'.$calories.LB;
      if ($calories != 500) {
        continue;
      }
      if ($score > $max_score){
        $max_score = $score;
      }
    }
  }
}

//for($i1=0; $i1<100; $i1++) {
//  $i2 = 100 - $i1;
//  if ($i2 < 0) {
//    continue;
//  }
//
//  $combination_string = $i1.'|'.$i2;
//  echo $combination_string . LB;
//  $combination = [$i1, $i2];
//  $score = score($combination);
//  echo '$score:'.$score.LB;
//  if ($score > $max_score){
//    $max_score = $score;
//  }
//}



// $combination: keys are ingredients, values are amounts.
function score($combination){
  global $infos;

  $capacity = 0;
  $durability = 0;
  $flavor = 0;
  $texture = 0;
  $calories = 0;

  foreach($combination as $ingredient => $amount){
    //print '$ingredient: '.$ingredient.', $amount:' . $amount . LB;
    $info = $infos[$ingredient]; //var_dump($info);
    $capacity += $info['capacity'] * $amount;
    $durability += $info['durability'] * $amount;
    $flavor += $info['flavor'] * $amount;
    $texture += $info['texture'] * $amount;
    $calories += $info['calories'] * $amount;
//    print '$capacity:'.$capacity.LB;
//    print '$durability:'.$durability.LB;
//    print '$flavor:'.$flavor.LB;
//    print '$texture:'.$texture.LB;
  }
  $score = ($capacity > 0 ? $capacity : 0) * ($durability > 0 ? $durability : 0) * ($flavor > 0 ? $flavor : 0) * ($texture > 0 ? $texture : 0);
  //echo '$score:' . $score . LB;
  return [$score, $calories];
}

//echo 'Test: ' . score(['Butterscotch' => 44, 'Cinnamon' => 56]);
echo '$max_score:' . $max_score . LB;