<pre>
<?php
/**
 * Created by PhpStorm.
 * User: ryan
 * Date: 12/6/15
 * Time: 12:58 AM
 */

//$raw = file_get_contents('aoc.5.txt');
//echo count($raw);

define('LB', "\n");

$raw = file('aoc.5.txt', FILE_IGNORE_NEW_LINES);
echo 'Starting with ' . count($raw) . LB;

$vowel_pattern = '/[^aeiou]/u';
$double_pattern = '/(aa|bb|cc|dd|ee|ff|gg|hh|ii|jj|kk|ll|mm|nn|oo|pp|qq|rr|ss|tt|uu|vv|ww|xx|yy|zz)/u';

$filtered = array_filter($raw, 'filter1');
echo 'Filtered down to ' . count($filtered) . ' lines' . LB;

$filtered2 = [];
foreach($filtered as $s) {
  $r = preg_replace($vowel_pattern, '', $s);
  if (strlen($r) >= 3) {
    $filtered2[] = $s;
  }
}
echo 'Filtered down to ' . count($filtered2) . ' lines' . LB;

$filtered3 = [];
foreach($filtered2 as $s) {
  if (preg_match($double_pattern, $s)) {
    $filtered3[] = $s;
    echo $s.LB;
  }
}
echo 'Filtered down to ' . count($filtered3) . ' lines' . LB;

function filter1($s) {

  if (strpos($s, 'ab') !== FALSE || strpos($s, 'cd') !== FALSE || strpos($s, 'pq') !== FALSE || strpos($s, 'xy') !== FALSE) {
    return FALSE;
  }

  return TRUE;
}



?>
</pre>
