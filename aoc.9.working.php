<?php
define('LB', "\n");

$raw = file('aoc.9.txt', FILE_IGNORE_NEW_LINES);
//echo 'Input contains ' . count($raw) . 'lines' . LB;

//London to Dublin = 464
$pattern = '/([^ ]+) to ([^ ]+) = (\d+)/';

$places = [];

$_distArr = array();
$tried_paths = [];

foreach($raw as $s) {
  if (preg_match($pattern, $s, $matches)) {
    var_dump($matches);
    $p1 = 0;
    $p2 = 0;

    if (!in_array($matches[1], $places)){
      $places[] = $matches[1];
    }
    $p1 = array_search($matches[1], $places);

    if (!in_array($matches[2], $places)){
      $places[] = $matches[2];
    }
    $p2 = array_search($matches[2], $places);

    //$_distArr[$p1][$p2] = $matches[3];
    $_distArr[$matches[1]][$matches[2]] = (int)$matches[3];
    echo $matches[1] . ' to ' . $matches[2] . ' = ' . $matches[3] . LB;
    $_distArr[$matches[2]][$matches[1]] = (int)$matches[3];
    echo $matches[2] . ' to ' . $matches[1] . ' = ' . $matches[3] . LB;
  }
}
echo LB.'$places'.print_r($places, TRUE).LB;
echo LB.'$_distArr'.print_r($_distArr, TRUE).LB;


$valid_paths = [];
for($i=0; $i<1000000; $i++) {
  $path = [];
  $dist = find_path_through_all_nodes($_distArr, $path);
  $path_s = implode('|', $path);
  if (isset($tried_paths[$path_s])){
    //skip
  }
  else{
    $tried_paths[$path_s] = $dist;
    echo '$dist:'.$dist.LB;
    $valid_paths[] = $dist;
  }
}
//$r = array_count_values($valid_paths);
//var_dump($r);
//print_r($valid_paths);
print_r($tried_paths);

$min = min($valid_paths);
$max = max($valid_paths);
echo '$min:'.$min.LB;
echo '$max:'.$max.LB;
exit();


function find_path_through_all_nodes($_distArr, &$path) {

  $all = array_keys($_distArr);

  shuffle($all);

  $dist = 0;
  $count = count($all);
  for($pi=0; $pi < $count-1; $pi++) {
    $a = $all[$pi];
    $b = $all[$pi+1];
    //echo '$a:'.$a.',$b'.$b.LB;
    $dist += $_distArr[$a][$b];
  }
  $path = $all;
  return $dist;
}



echo LB.'$valid_paths'.LB;
var_dump($valid_paths);
