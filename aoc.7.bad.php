<pre>
<?php

define('LB', "\n");

// Read as a string
//$raw = file_get_contents('aoc.7.txt');

// Or, read as array
$raw = file('aoc.7.txt', FILE_IGNORE_NEW_LINES);
echo 'Starting with ' . count($raw) . LB;

//$pattern = '/(([a-z]+|\d+))\w?(\w+)\w?(([a-z]+|\d+)) -> ([a-z]+)/u';
//$pattern = '/([^ ]*)\w?(\w+)\w?(([a-z]+|\d+)) -> ([a-z]+)/u';
//$pattern = '/([^ ]*).*([a-z]+)/u';
$pattern = '/(.*) -> ([a-z]+)/u';
$pattern_input = '/([^ ]*)\w?()\w?([^ ]*)/u';

global $wires;
$wires = [];


function getSignalValue($s, $wires) {
  if (is_numeric($s)) {
    return (int)$s;
  }else {
    if (isset($wires[$s])){
      return $wires[$s];
    }else{
      return 0;
    }
  }
}


foreach ($raw as $s) {

  $result_signal = 0;
  $matches = [];
  preg_match($pattern, $s, $matches);
  //var_dump($matches);

  $input = $matches[1];
  $output = $matches[2];
  echo "input: $input, output: $output\n";

//  $matches = [];
//  preg_match($pattern_input, $input, $matches);
//  var_dump($matches);

  $input_components = explode(' ', $input);
  print_r($input_components);

  if (count($input_components) == 3) {
    $a = getSignalValue($input_components[0], $wires);
    $gate = $input_components[1];
    $b = getSignalValue($input_components[2], $wires);

    echo '$a: ' . $a . ', $b: ' . $b . LB;

    if ($gate == 'AND') {
      $result_signal = $a & $b;

    }elseif ($gate == 'OR') {
      $result_signal = $a | $b;

    }elseif ($gate == 'LSHIFT') {

      $result_signal = $a << $b;

    }elseif ($gate == 'RSHIFT') {

      $result_signal = $a >> $b;

    }else{
      echo 'Error, unrecognized gate for 3 components'.LB; exit();
    }

  }elseif (count($input_components) == 2) {

    $gate = $input_components[0];
    $a = getSignalValue($input_components[1], $wires);

    if ($gate == 'NOT') {
      $result_signal = ~$a;
    }else{
      echo 'Error, unrecognized gate for 2 components'.LB; exit();
    }

  }elseif (count($input_components) == 1) {
    $a = $input_components[0];
    $result_signal = getSignalValue($a, $wires);

  }else{
    echo 'Error, too many input components'.LB; exit();

  }
  $result_signal = $result_signal & 65535;

  echo '$result_signal: ' . $result_signal . LB;
  $wires[$output] = $result_signal;

  //var_dump($wires);
  printWires($wires);

}

ksort($wires);
echo LB.LB;
printWires($wires);


function printWires($wires) {

  foreach($wires as $w => $signal) {
    //$v = sprintf("%u\n", $signal);
    //echo $w . ' ' . $v;

    echo $w . ' = ' . ($signal & 65535) . LB;

//
//  if ($signal > 65535) {
//    $signal =
//  }
  }
}


return;
var_dump($r);

// Print result

echo LB . '' . LB;

?>
</pre>
