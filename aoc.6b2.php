<pre>
<?php
/**
 * Created by PhpStorm.
 * User: ryan
 * Date: 12/6/15
 * Time: 8:25 AM
 */

define('LB', "\n");
define('L', 1000);

function getCellID($_x, $_y) {
  return (max(0, $_y) * L) + $_x;
}

// Read as a string
$raw = file_get_contents('aoc.6.txt');
$raw = str_replace('toggle', 't', $raw);
$raw = str_replace('turn on', 'o', $raw);
$raw = str_replace('turn off', 'f', $raw);
$raw = str_replace(' through ', '-', $raw);

// Or, read as array
//$raw = file('aoc.6.txt', FILE_IGNORE_NEW_LINES);
echo 'Starting with ' . count($raw) . LB;

$lights = [];
$lights = array_fill(0,1000000, 0);

//$i_pattern = '/(toggle|turn on|turn off) ()/u';
$i_pattern = '/(\w{1}) (\d*),(\d*)-(\d*),(\d*)/u';

foreach($raw as $instruction) {


  echo $instruction . LB;

  $matches = [];
  preg_match($i_pattern, $instruction, $matches);
  //var_dump($matches);

  $action = $matches[1];
  $x1 = $matches[2];
  $y1 = $matches[3];
  $x2 = $matches[4];
  $y2 = $matches[5];


  $effect = 0;

  //$startCellID = getCellID($x1, $y1); echo $startCellID.LB;
  //$endCellID = getCellID($x2, $y2); echo $endCellID.LB;
//  for($i=$startCellID; $i<=$endCellID; $i++) {
//    $effect++;
//    if ($action == 't') {
//      $lights[$i] = 1 - $lights[$i];
//    }elseif  ($action == 'o') {
//      $lights[$i] = 1;
//    }elseif  ($action == 'f') {
//      $lights[$i] = 0;
//    }
//  }

  for($y=$y1; $y<=$y2; $y++) {
    for($x=$x1; $x<=$x2; $x++) {
      $effect++;
      $cellID = getCellID($x, $y); //echo $cellID.LB;
      if ($action == 't') {
        $lights[$cellID] = $lights[$cellID] + 2;
      }elseif  ($action == 'o') {
        $lights[$cellID]++;
      }elseif  ($action == 'f') {
        $lights[$cellID]--;
        if ($lights[$cellID] < 0){
          $lights[$cellID] = 0;
        }
      }
    }
  }

  //echo 'Affected '.$effect.' lights'.LB;

}

$r = array_count_values($lights);
var_dump($r);

$total = 0;
foreach ($lights as $l) {
  $total += $l;
}


// Print result

echo LB . 'Total brightness = ' . $total . LB;

?>
</pre>
