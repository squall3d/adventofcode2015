<pre>
<?php
/**
 * Created by PhpStorm.
 * User: ryan
 * Date: 12/6/15
 * Time: 12:09 AM
 */

define('LB', "\n");

$raw = file_get_contents('aoc.3.txt');
//$raw = '^v^v^v^v^v';
//echo $raw;
$raw = str_split($raw);
//echo $raw;

$presents = [];

$x = 3000;
$y = 3000;
$rx = 3000;
$ry = 3000;
$turn = 0; //0 for santa, 1 for roboSanta;
define('L', 6000);

function getCellID($_x, $_y) {
  return (max(0, $_y - 1) * L) + $_x - 1;
}

$startCellID = getCellID($x, $y); //echo $startCellID . LB;
$presents[$startCellID] = 2;

$errors = 0;

foreach($raw as $dir) {

  // Robo
  if ($turn) {

    if ($dir == '<') {
      $rx--;
    }elseif ($dir == '^') {
      $ry--;
    }elseif ($dir == '>') {
      $rx++;
    }elseif ($dir == 'v') {
      $ry++;
    }else{
      $errors++;
    }
    $cellId = getCellID($rx, $ry); echo 'Robo $cellId:' . $cellId . LB;
    $presents[$cellId]++;
  }
  // Santa
  else{

    if ($dir == '<') {
      $x--;
    }elseif ($dir == '^') {
      $y--;
    }elseif ($dir == '>') {
      $x++;
    }elseif ($dir == 'v') {
      $y++;
    }else{
      $errors++;
    }
    $cellId = getCellID($x, $y); echo 'Santa $cellId:' . $cellId . LB;
    $presents[$cellId]++;
  }
  $turn = 1 - $turn;
}

echo 'houses with at least 1 present: ' . countHousesAtLeastOnePresent($presents) . "\n";
echo 'Errors: ' . $errors . "\n";

function countHousesAtLeastOnePresent($houses) {
  $c = 0;
  //$c2 = 0;
  foreach($houses as $key => $house) {
    if ($house > 0) {
      $c++;
      echo $key . "\n";
//      if ($house > 1) {
//        $c2++;
//      }
    }
  }

  //echo LB . 'houses with at least 2 present: ' . $c2 . LB;
  return $c;
}

?>
</pre>
