<?php
/**
 * Created by PhpStorm.
 * User: ryan
 * Date: 12/6/15
 * Time: 1:23 AM
 */
define('LB', "\n");
$raw = file_get_contents('aoc.1.txt');
//$raw = '()())';
$raw = str_split($raw);

$level = 0;
$i = 0;
$position_first_basement = -1;

foreach($raw as $s) {
  $i++;
  if ($s == '('){
    $level++;
  }elseif ($s == ')') {
    $level--;
  }
  if ($level < 0 && $position_first_basement < 0) {
    $position_first_basement = $i;
  }
}

if ($position_first_basement) {
  echo 'First position to get to basement:' . $position_first_basement . LB;
}
echo 'Ending floor='.$level;