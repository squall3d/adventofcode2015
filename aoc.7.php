<pre>
<?php

define('LB', "\n");

// Read as a string
//$raw = file_get_contents('aoc.7.txt');

// Or, read as array
$raw = file('aoc.7.txt', FILE_IGNORE_NEW_LINES);
echo 'Starting with ' . count($raw) . LB;

//$pattern = '/(([a-z]+|\d+))\w?(\w+)\w?(([a-z]+|\d+)) -> ([a-z]+)/u';
//$pattern = '/([^ ]*)\w?(\w+)\w?(([a-z]+|\d+)) -> ([a-z]+)/u';
//$pattern = '/([^ ]*).*([a-z]+)/u';
$pattern = '/(.*) -> ([a-z]+)/u';
$pattern_input = '/([^ ]*)\w?()\w?([^ ]*)/u';

global $wires;
$wires = [];

foreach ($raw as $s) {

  $result_signal = 0;
  $matches = [];
  preg_match($pattern, $s, $matches);
  //var_dump($matches);

  $input = $matches[1];
  $output = $matches[2];
  echo "input: $input, output: $output\n";

  $input_components = explode(' ', $input);
  print_r($input_components);

  $wires[$output] = $input_components;
}
echo LB.LB;
printWires($wires);
echo LB.LB;

$cache_values = [];

function getSignalValue($wire, $wires, &$cache_values) {

  // Check cache.
  if (isset($cache_values[$wire])){
    return $cache_values[$wire];
  }


  if (is_numeric($wire)) {
    $cache_values[$wire] = (int)$wire;
    return (int)$wire;
  }

  if (!isset($wires[$wire])) {
    echo 'Wire not found.'; exit();
  }

  $input_components = $wires[$wire];

  $result_signal = 0;

  if (count($input_components) == 3) {
    $a = getSignalValue($input_components[0], $wires, $cache_values);
    $gate = $input_components[1];
    $b = getSignalValue($input_components[2], $wires, $cache_values);

    echo "$a $gate $b" . LB;

    if ($gate == 'AND') {
      $result_signal = $a & $b;
    }elseif ($gate == 'OR') {
      $result_signal = $a | $b;
    }elseif ($gate == 'LSHIFT') {
      $result_signal = $a << $b;
    }elseif ($gate == 'RSHIFT') {
      $result_signal = $a >> $b;
    }/*else{
      echo 'Error, unrecognized gate for 3 components'.LB; exit();
    }*/

  }elseif (count($input_components) == 2) {

    $gate = $input_components[0];
    $a = getSignalValue($input_components[1], $wires, $cache_values);

    if ($gate == 'NOT') {
      $result_signal = ~$a;
    }/*else{
      echo 'Error, unrecognized gate for 2 components'.LB; exit();
    }*/

  }elseif (count($input_components) == 1) {
    $a = $input_components[0];
    $result_signal = getSignalValue($a, $wires, $cache_values);
  }/*else{
    echo 'Error, too many input components'.LB; exit();
  }*/


  $result_signal = $result_signal & 65535;
  $cache_values[$wire] = $result_signal;

  echo '$result_signal for ' . $wire . ' = ' . $result_signal . LB;

  return $result_signal;
}

echo LB.LB;
echo getSignalValue('a', $wires, $cache_values);
echo LB.LB;
var_dump($cache_values);
printWires($cache_values);


function printWires($array) {
  foreach($array as $k => $v) {
    if (is_array($v)) {
      echo $k . ' = ' . implode(',', $v) . LB;
    }
    else{
      echo $k . ' = ' . $v . LB;
    }
  }
}


return;