<?php

define('LB', "\n");

// Read as a string
$raw = file_get_contents('aoc.12.txt');
$object = json_decode($raw);

function addNumbers($obj){

  if (gettype($obj) === 'integer'){
    return (int)$obj;
  }
  elseif (gettype($obj) === 'string'){
    if ($obj === 'red') {
      return FALSE;
    }
    else{
      return 0;
    }
  }
  elseif (gettype($obj) === 'array'){
    $c = 0;
    foreach ($obj as $v){
      $c += addNumbers($v);
    }
    return $c;
  }
  elseif (gettype($obj) === 'object'){
    $c = 0;
    foreach ($obj as $v){
      if (gettype($v) === 'string' && $v === 'red'){
        return 0;
      }
      $c += addNumbers($v);
    }
    return $c;
  }else{
    return 0;
  }

}

$total = addNumbers($object);
print '$total:'.$total;