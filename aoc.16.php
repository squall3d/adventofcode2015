<?php
define('LB', "\n");

$raw = file('aoc.16.txt', FILE_IGNORE_NEW_LINES);
echo 'Input contains ' . count($raw) . 'lines' . LB;

//Sue 460: akitas: 1, cars: 5, children: 8
$pattern = '/Sue (\d+): (.+)/';
$pattern2 = '/([^ ]+): ([-\d]+)/';

$sues = [];

$target = [
  'children' => 3,
  'cats' => 7,
  'samoyeds' => 2,
  'pomeranians' => 3,
  'akitas' => 0,
  'vizslas' => 0,
  'goldfish' => 5,
  'trees' => 3,
  'cars' => 2,
  'perfumes' => 1,
];

foreach ($raw as $line) {
  if (preg_match($pattern, $line, $matches_outer)) {
    //print_r($matches_outer);

    if (preg_match_all($pattern2, $matches_outer[2], $matches)){
      //echo 'Sue' . $matches_outer[1] . ':' . print_r($matches, TRUE) . LB;

      $props = ['id' => $matches_outer[1]];
      $not_this_sue = FALSE;
      for($i=0; $i<count($matches[1]); $i++) {

        $prop = $matches[1][$i];
        $value = $matches[2][$i];
        $props[$prop] = $value;

        if (isset($target[$prop])) {
          if ($prop == 'cats' || $prop == 'trees') {
            if ($value <= $target[$prop]){
              $not_this_sue = TRUE;
            }
          }elseif ($prop == 'pomeranians' || $prop == 'goldfish') {

            if ($value >= $target[$prop]){
              $not_this_sue = TRUE;
            }
          }else{
            if ($value != $target[$prop]){
              $not_this_sue = TRUE;
            }
          }
        }

      }
      //$sues[(int)$matches_outer[1]] = $props;
      if ($not_this_sue) {
        continue;
      }
      $sues[] = $props;


    }
  }
}

echo 'Start: ' . print_r($sues, TRUE) . LB;