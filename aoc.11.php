<?php
define('LB', "\n");


$pass = 'hxbxwxba';
$pass = 'hxbxxyzz';

$pairs_pattern = '/(\w)\1/s';
$triplets = [
  'abc',
  'bcd',
  'cde',
  'def',
  'efg',
  'fgh',
  'ghi',
  'hij',
  'ijk',
  'jkl',
  'klm',
  'lmn',
  'mno',
  'nop',
  'opq',
  'pqr',
  'qrs',
  'rst',
  'stu',
  'tuv',
  'uvw',
  'vwx',
  'wxy',
  'xyz',
];
$triples_pattern = '/(abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mno|nop|opq|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz)/s';

if (isset($argv[1])) {
  $pass = $argv[1];
}

echo '$pass:' . $pass . LB;

$found = 0;
$start = microtime(TRUE) * 1000;
for ($r = 0; $r < 10000000; $r++) {
  $pass = getNextPassword($pass);
  //if (isValidPassOld($pass)) {
  if (isValidPass($pass)) {
    $found = 1;
    echo 'Found after ' . $r . ' loops.' . LB;
    break;
  }

  echo 'Loop ' . $r . ' (' . $pass . ')' . LB;
}
$time_taken = (microtime(TRUE) * 1000) - $start;
echo 'Time taken: ' . $time_taken . LB;

if ($found) {
  echo 'New pass:' . $pass . LB;
}
else {
  echo 'Not found.' . LB;
}

function getNextPassword($pass) {
  $pass_arr = str_split($pass);

  $carry = 1;
  for ($i = count($pass_arr) - 1; $i >= 0; $i--) {

    if ($carry == 1) {
      $carry = 0;
      if ($pass_arr[$i] == 'h') {
        $pass_arr[$i] = 'j';
      }
      elseif ($pass_arr[$i] == 'k') {
        $pass_arr[$i] = 'm';
      }
      elseif ($pass_arr[$i] == 'n') {
        $pass_arr[$i] = 'p';
      }
      elseif ($pass_arr[$i] == 'z') {
        $pass_arr[$i] = 'a';
        $carry = 1;
      }
      else {
        $pos_value = ord($pass_arr[$i]);
        $pass_arr[$i] = chr($pos_value + 1);
      }
    }
    else {
      break;
    }

  }

  $new_pass = implode('', $pass_arr);
  return $new_pass;
}

function check2PairsOld($pass_arr) {

  $found = 0;
  for ($i = 0; $i < count($pass_arr) - 1; $i++) {
    $a = ord($pass_arr[$i]);
    $b = ord($pass_arr[$i + 1]);
    if ($a == $b) {
      $found++;
      $i++;
      if ($found == 2) {
        break;
      }
    }
  }
  if ($found !== 2) {
    return FALSE;
  }

  return TRUE;
}

function check2Pairs($pass) {
  global $pairs_pattern;

  if (preg_match_all($pairs_pattern, $pass, $matches)) {
    //var_dump($matches);
    if (count($matches[0]) > 1) {
      return TRUE;
    }
  }
  return FALSE;
}


function isValidPassOld($pass) {

  if (strpos($pass, 'i') !== FALSE
    || strpos($pass, 'l') !== FALSE
    || strpos($pass, 'o') !== FALSE
  ) {
    return FALSE;
  }

  $pass_arr = str_split($pass);

  $found = 0;
  for ($i = 0; $i < strlen($pass) - 2; $i++) {
    $a = ord($pass_arr[$i]);
    $b = ord($pass_arr[$i + 1]);
    $c = ord($pass_arr[$i + 2]);
    if ($c == ($b + 1) && $b == ($a + 1)) {
      $found = 1;
      break;
    }
  }
  if ($found === 0) {
    return FALSE;
  }

  $has_2_pairs = check2PairsOld($pass_arr);

  return $has_2_pairs;

}

function isValidPass($pass) {
  global $triples_pattern;

  if (strpos($pass, 'i') !== FALSE
    || strpos($pass, 'l') !== FALSE
    || strpos($pass, 'o') !== FALSE
  ) {
    return FALSE;
  }

  $has_triples = preg_match($triples_pattern, $pass);
  if (!$has_triples) {
    return FALSE;
  }

  $has_2_pairs = check2Pairs($pass);

  return $has_2_pairs;

}
