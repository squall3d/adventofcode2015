<pre>
<?php
/**
 * Created by PhpStorm.
 * User: ryan
 * Date: 12/6/15
 * Time: 12:58 AM
 */

//$raw = file_get_contents('aoc.5.txt');
//echo count($raw);

define('LB', "\n");

$raw = file('aoc.5.txt', FILE_IGNORE_NEW_LINES);
echo 'Starting with ' . count($raw) . LB;

$pair_twice_pattern = '/(\w{2})\w*\1/u';
$letter_twice_pattern = '/(\w{1})\w{1}\1/u';

$filtered = [];
foreach($raw as $s) {
  $matches = [];
  if (preg_match($pair_twice_pattern, $s, $matches)){
    $filtered[] = $s;
  }
  var_dump($matches);
}
echo 'Filtered down to ' . count($filtered) . ' lines' . LB;

$filtered2 = [];
foreach($filtered as $s) {
  $matches = [];
  if (preg_match($letter_twice_pattern, $s, $matches)){
    $filtered2[] = $s;
  }
  var_dump($matches);
}
echo 'Filtered down to ' . count($filtered2) . ' lines' . LB;



?>
</pre>
