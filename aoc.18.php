<?php
define('LB', "\n");
define('L', 100);
define('STEPS', 100);

$rows = file('aoc.18.txt', FILE_IGNORE_NEW_LINES);
echo 'Input contains ' . count($rows) . 'lines' . LB;


$lights = [];
$y = 0;

foreach($rows as $row) {
  $row = str_split($row);
  $x=0;
  foreach($row as $light) {
    //$lights[] = $light == '#' ? TRUE : FALSE;
    $lights[$x++.'|'.$y] = ($light == '#' ? 1 : 0);
  }
  $y++;
}
echo 'Start: ' . print_r($lights, TRUE) . LB;

for($i=0; $i<STEPS; $i++) {
  echo 'Step ' . $i . LB;

  $new_state = [];
  for($y=0; $y<L; $y++) {
    for($x=0; $x<L; $x++) {
      $neighbours_on = countNeighbours($x, $y, $lights);
      if ($lights[$x.'|'.$y] === 1) {
        if ($neighbours_on == 2 || $neighbours_on == 3){
          $new_state[$x.'|'.$y] = 1;
        } else{
          $new_state[$x.'|'.$y] = 0;
        }
      }else{
        if ($neighbours_on == 3){
          $new_state[$x.'|'.$y] = 1;
        } else{
          $new_state[$x.'|'.$y] = 0;
        }
      }
    }
  }
  $new_state['0|0'] = 1;
  $new_state['0|99'] = 1;
  $new_state['99|0'] = 1;
  $new_state['99|99'] = 1;
  //echo 'New state' . print_r($new_state, TRUE).LB;
  $lights = $new_state;

}

$on = 0;
foreach($lights as $light) {
  if ($light === 1) {
    $on++;
  }
}
echo 'On:'.$on.LB;

function countNeighbours($x, $y, $lights) {
  $neighbours = [
    ($x-1).'|'.($y-1),
    ($x).'|'.($y-1),
    ($x+1).'|'.($y-1),
    ($x-1).'|'.($y),
    ($x+1).'|'.($y),
    ($x-1).'|'.($y+1),
    ($x).'|'.($y+1),
    ($x+1).'|'.($y+1),
  ];
//  $neighbours = array_filter($neighbours);
//  $on = array_sum($neighbours);
  $on = 0;
  foreach($neighbours as $neighbour) {
    if (isset($lights[$neighbour]) && $lights[$neighbour] === 1) {
      $on++;
    }
  }
  return $on;
}

//function getCellID($_x, $_y) {
//  return (max(0, $_y) * L) + $_x;
//}