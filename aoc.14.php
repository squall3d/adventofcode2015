<?php
define('LB', "\n");

$raw = file('aoc.14.txt', FILE_IGNORE_NEW_LINES);
echo 'Input contains ' . count($raw) . 'lines' . LB;

//Vixen can fly 8 km/s for 8 seconds, but then must rest for 53 seconds.
$pattern = '/([^ ]+) can fly (\d+) km\/s for (\d+) seconds, but then must rest for (\d+) seconds./';

$deers = [];

foreach ($raw as $deer) {
  if (preg_match($pattern, $deer, $matches)) {
    //print_r($matches);
    $deers[$matches[1]] = [
      'name' => $matches[1],
      'speed' => $matches[2],
      'fly' => $matches[3],
      'rest' => $matches[4],
      'position' => 0,
      'fly_cd' => $matches[3],
      'rest_cd' => 0,
      'flying' => TRUE,
      'points' => 0,
    ];
  }
}

echo 'Start: ' . print_r($deers, TRUE) . LB;

for($i=0; $i<2503; $i++) {

  $lead_position = 0;

  foreach($deers as $name => &$deer) {
    if ($deer['flying']) {
      if ($deer['fly_cd'] > 0) {
        $deer['position'] += $deer['speed'];
        $deer['fly_cd']--;
        if ($deer['fly_cd'] === 0) {
          $deer['flying'] = FALSE;
          $deer['rest_cd'] = $deer['rest'];
        }
      }else{
        echo 'Invalid state while flying.'.LB;
      }
    }else{
      if ($deer['rest_cd'] > 0) {
        $deer['rest_cd']--;
        if ($deer['rest_cd'] === 0) {
          $deer['flying'] = TRUE;
          $deer['fly_cd'] = $deer['fly'];
        }
      }else{
        echo 'Invalid state while resting.'.LB;
      }
    }

    if ($deer['position'] > $lead_position) {
      $lead_position = $deer['position'];
    }

  }

  foreach($deers as &$deer) {
    if ($deer['position'] === $lead_position){
      $deer['points']++;
    }
  }
}


//echo 'End: ' . print_r($deers, TRUE) . LB;

foreach($deers as &$deer) {
  print $deer['name'].LB;
  //print $deer['position'].LB;
  print $deer['points'].LB;
}