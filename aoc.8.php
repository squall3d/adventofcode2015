<?php
define('LB', "\n");

$raw = file('aoc.8.ori.txt', FILE_IGNORE_NEW_LINES);
//echo 'Input contains ' . count($raw) . 'lines' . LB;

$hex_pattern = '/(\\\\x[0-9a-f]{2})/';
$totalcode = 0;
$total = 0;

foreach($raw as $s) {

  $totalcode += strlen($s);
  // echo "$s (" . strlen($s) . ")\n";

  $s = substr($s, 1, strlen($s) - 2);
  $s = str_replace('\\\\', '\\', str_replace('\"', '"', $s));

  // If we're interested to see matches.
  //$matches = [];
  //if (preg_match($hex_pattern, $s, $matches)){
  //  var_dump($matches);
  //}

  // If we're interested to know how many got replaced.
  //$count = 0;
  $s = preg_replace($hex_pattern, 'H', $s, -1, $count);
  //echo 'Replaced ' . $count . ' hex characters.' . LB;

  $total += strlen($s);
  //echo "$s (" . strlen($s) . ")\n";
}


echo "totalcode: $totalcode, total: $total, ans: " . ($totalcode-$total) . ".";