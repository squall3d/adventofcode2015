<pre>
<?php

define('LB', "\n");

$key = 'ckczppom';
$i = 0;
$answer = 0;
$md5 = '';

$time = microtime(TRUE) * 1000;

for(;$i<10000000; $i++) {
  $test = $key . $i; //echo $test.LB;
  $md5 = md5($test); //echo $md5.LB;
  if (substr( $md5, 0, 6 ) === "000000") {
    $answer = $i;
    break;
  }
}

if ($answer) {
  echo 'Answer is ' . $answer . ', md5 = ' . $md5 . LB;
}
else{
  echo 'Did not find an answer';
}
echo 'Time taken ' . ((microtime(TRUE) * 1000) - $time) . 'ms';

?>
</pre>
