<?php
define('LB', "\n");

$raw = file('aoc.9.ws.txt', FILE_IGNORE_NEW_LINES);
//echo 'Input contains ' . count($raw) . 'lines' . LB;

//London to Dublin = 464
$pattern = '/([^ ]+) to ([^ ]+) = (\d+)/';

// Process places and distances.
$_distArr = array();
foreach($raw as $s) {
  if (preg_match($pattern, $s, $matches)) {
    $_distArr[$matches[1]][$matches[2]] = (int)$matches[3];
    $_distArr[$matches[2]][$matches[1]] = (int)$matches[3];
  }
}

$tried_paths = [];
for($i=0; $i<1000000; $i++) {

  $path = [];
  $dist = find_path_through_all_nodes($_distArr, $path);
  $path_s = implode('|', $path);

  if (!isset($tried_paths[$path_s])){
    $tried_paths[$path_s] = $dist;
    echo '$dist:'.$dist.LB;
  }
}
print_r($tried_paths);

$min = min($tried_paths);
$max = max($tried_paths);
echo '$min:'.$min.LB;
echo '$max:'.$max.LB;
exit();


function find_path_through_all_nodes($_distArr, &$path) {
  $all = array_keys($_distArr);
  shuffle($all);

  $dist = 0;
  $count = count($all);
  for($pi=0; $pi < $count-1; $pi++) {
    $dist += $_distArr[$all[$pi]][$all[$pi+1]];
  }
  $path = $all;
  return $dist;
}
